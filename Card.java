enum Suit {
    Hearts(0.4),
    Spades(0.3), 
    Diamonds(0.2), 
    Clubs(0.1) ;

    private final double score;

    Suit(double score) {
        this.score = score;
    }

    public double getScore() {
        return score;
    }
}

enum Value {
    Ace(1.0),
    Two(2.0), 
    Three(3.0), 
    Four(4.0), 
    Five(5.0), 
    Six(6.0), 
    Seven(7.0), 
    Eight(8.0), 
    Nine(9.0), 
    Ten(10.0), 
    Jack(11.0), 
    Queen(12.0),
    King(13.0);

    private final double score;

    Value(double score) {
        this.score = score;
    }

    public double getScore() {
        return score;
    }
}

public class Card {
    private Suit suit;
    private Value value;
    
    public Card (Suit suit, Value value) {
        this.suit = suit;
        this.value = value;
    }

    public Suit getSuit() {
		return this.suit;
	}

    public Value getValue() {
		return this.value;
	}
     public String toString() {
        return value + " of " + suit;
    }

	public Double calculateScore() {
        return value.getScore() + suit.getScore();
	}

}
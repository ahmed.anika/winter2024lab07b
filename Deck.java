import java.util.Random;

public class Deck {
    private Card[] cards;
    private int numberOfCards;
    private Random rng;

    public Deck() {
        this.rng = new Random();
        this.cards = new Card[52];
        this.numberOfCards = 52;

        int i = 0;
        for (Value v : Value.values()) {
            for (Suit s : Suit.values()) {
                cards[i] = new Card(s, v);
                i++;
            }
        }
    }

    public int length() {
        return this.numberOfCards;
    }

    public Card drawTopCard() {
        if (numberOfCards > 0) {
            numberOfCards--;
            return cards[numberOfCards];
        } else {
            return null;
        }
    }

    public String toString() {
		String s = "Your deck is " + this.cards.length + " long. Here are the cards: \n";
		for (Card c : this.cards) {
			s += c.toString() + "\n";
		}
		return s;
	}
	
	public void shuffle() {
        for (int i = 0; i < this.numberOfCards - 1; i++) {
            int index = rng.nextInt(numberOfCards);
            Card current = cards[i];
            cards[i] = cards[index];
            cards[index] = current;
		}
	}

}